# Semaa

## Motivation

**Semaa** is a personal project which I am developing for enhancing my skills in react native and connecting with my passion for space and astronomy. 

## Description

**Semaa** is a cross platform app which gives news and alerts about space events, APOD(Astronomy Picture of the Day), information about spacecraft and much more.Yeah I know there are tons of apps out there providing better info, but not so fast I am planning to support multilanguages and also apply translator to the info getting from apis, Providing special care for urdu.

## Technicalities

*Developed using*

- React Native
- Typescript
- Realm & Firebase
- Open Source space apis 
  - [NASA](https://api.nasa.gov/)
  - [SpaceX - GraphQL](https://api.spacex.land/graphql/) or [SpaceX - RestApi](https://github.com/r-spacex/SpaceX-API)
  - [Launch Library 2](https://thespacedevs.com/llapi)


## Project Roadmap

- [ ] Feed
- [ ] Profile

## Implementation 

- [ ] UI Layout
- [ ] Tests
- [ ] Debugging
- [ ] CI/CD
     - [X] Husky - Git Hooks -> used for linting and tests
- [ ] API Integration
- [ ] State Manager
